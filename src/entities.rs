pub mod client;

use serde::{de::DeserializeOwned, Serialize};

pub trait Entity: Send + Unpin + Sync + Serialize + DeserializeOwned + Clone {
    fn id(self) -> String;
}
