use serde::{Deserialize, Serialize};

use super::Entity;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Client {
    pub id: String,
    pub template_id: String,
}

impl Entity for Client {
    fn id(self) -> String {
        self.id
    }
}
