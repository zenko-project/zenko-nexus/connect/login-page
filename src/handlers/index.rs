use axum::{
    extract::{Query, State},
    response::Html,
};
use handlebars::Handlebars;
use serde::Serialize;
use std::sync::Arc;
use tracing::debug;

use crate::{app::state::AppState, dtos};

#[derive(Serialize)]
struct PageData;

pub async fn handle(
    State(state): State<Arc<AppState>>,
    Query(query): Query<dtos::index::Query>,
) -> Result<Html<String>, ()> {
    debug!("Query: {:?}", query);

    let page_template = "";
    let page_data = PageData;

    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_string("page", page_template)
        .unwrap();
    let html = handlebars.render("page", &page_data).unwrap();

    Ok(Html(html))
}
