pub mod app;
pub mod dtos;
pub mod entities;
pub mod errors;
pub mod handlers;
pub mod log;
pub mod repositories;
pub mod routes;
pub mod settings;

use app::App;

#[tokio::main]
async fn main() {
    let mut app = App::new().expect("Cannot create App");

    app.start().await.expect("App Crashed");
}
