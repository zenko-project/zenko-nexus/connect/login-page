use std::sync::Arc;

use axum::{routing::get, Router};

use crate::{app::state::AppState, handlers::index};

pub fn create_routes(state: Arc<AppState>) -> Router {
    Router::new()
        .route("/", get(index::handle))
        .with_state(state)
}
