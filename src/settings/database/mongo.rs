use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct MongoSetting {
    pub uri: String,
    pub database: String,
    pub todo_collection: String,
}
